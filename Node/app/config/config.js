var config = {};

config.DB = {
    host: 'localhost',
    user: 'root',
    password: '',
    Localdatabase: 'practicaldemo',
    Testdatabase: 'practicaldemo',
    Livedatabase: 'practicaldemo',
    dialect: 'mysql'
}

module.exports = config;