//var mongoose = require("mongoose");
const mysql = require('mysql');
//const User = require('../../../models/user');
const User = require('../../models/post');
const db = require('../../models');

const Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');


// Create and Save a New Record
exports.create = async (req, res) => {
    if (!req.body) {
        return res.status(200).send({
            status: 0,
            message: "content can not be empty"
        });
    }

    // Create a record
    console.log('req.body', req.body);
    const object = req.body;

    object.CreatedAt = new Date();
    object.CreatedBy = 1;
    db[req.params.collection].create(object)
        .then(data => {
            var message = 'Created Successfully!';

            res.json({ status: 1, message: message, data: data });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });
};

// Retrieve and return all records from the collection.
exports.get = async (req, res) => {
    return db[req.params.collection].findAll({
        include: [{
            all: true,
            nested: false
        }],
        where: {
            IsActive: true
        }
    }).then((contacts) =>
        res.json({ status: 1, message: "Success", data: contacts }))
        //res.send(contacts))
        .catch((err) => {
            console.log('There was an error !', JSON.stringify(err))
            return res.send(err)
        });
};

// Find a single record with a id
exports.getById = (req, res) => {
    const decryptedID = req.params.id;
    return db[req.params.collection].findOne({
        include: [{ all: true, nested: true }],
        where: {
            id: decryptedID,
            IsActive: true
        }
    }).then(data => {
        if (!data) {
            var message = 'Record not found with id ';
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        res.json({ status: 1, message: "Success", data: data });
        //  res.send(data);
    }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID + " " + err
            });
        }
        return res.status(500).send({
            message: "Error retrieving record with id " + decryptedID + " " + err
        });
    });
};

// Update a record identified by the id in the request
exports.update = (req, res) => {
    const decryptedID = req.params.id;
    if (!req.body) {
        return res.status(400).send({
            message: "Record content can not be empty"
        });
    }
    else {
        db[req.params.collection].findByPk(decryptedID)
            .then(data => {
                if (!data) {
                    var message = 'Record not found with id ';
                    return res.status(200).send({
                        status: 0,
                        message: message + decryptedID
                    });
                }
                // Check if record exists in db
                else {
                    // Find record and update it with the request body
                    data.update(req.body)
                        .then(data => {
                            if (!data) {
                                var message = 'Record not found with id ';
                                return res.status(200).send({
                                    status: 0,
                                    message: message + decryptedID
                                });
                            }
                            data.update({
                                ModifiedAt: new Date(),
                                ModifiedBy: 1
                            });
                            var message = 'Updated Successfully! ';
                            res.json({ status: 1, message: message, data: data });
                            // res.send(data);
                        }).catch(err => {
                            if (err.kind === 'ObjectId') {
                                return res.status(404).send({
                                    message: "Record not found with id " + decryptedID
                                });
                            }
                            return res.status(500).send({
                                message: "Error updating record with id " + decryptedID
                            });
                        });
                }
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Record not found with id " + decryptedID
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving record with id " + decryptedID
                });
            });
    }
};

// Delete a record with the specified id in the request
exports.destroy = (req, res) => {
    const decryptedID = req.params.id;

    db[req.params.collection].destroy({
        where: {
            id: decryptedID
        }
    }).then(data => {
        console.log("sucess");
        if (!data) {
            var message = 'Record not found with id ';
            return res.status(200).send({
                status: 0,
                message: message + decryptedID,
                data: null
            });
        }
        res.json({ status: 1, message: 'Succesvol verwijderd!', data: data });
    }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Could not delete record with id " + decryptedID
        });
    });
};

exports.softdestroy = (req, res) => {
    const decryptedID = req.params.id;

    db[req.params.collection].update({ IsActive: false }, {
        where: {
            id: decryptedID
        }
    }).then(data => {
        console.log("sucess");
        if (!data) {
            var message = 'Record not found with id ';
            return res.status(200).send({
                status: 0,
                message: message + decryptedID,
                data: null
            });
        }
        var message = 'Successfully deleted!';
        return res.status(200).send({
            status: 1,
            message: message,
            data: data
        });
    }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Could not delete record with id " + decryptedID
        });
    });
};