-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2020 at 10:36 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `practicaldemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblrole`
--

CREATE TABLE `tblrole` (
  `Id` bigint(121) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` bigint(121) NOT NULL,
  `ModifiedAt` datetime NOT NULL,
  `ModifiedBy` bigint(121) NOT NULL,
  `IsActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblrole`
--

INSERT INTO `tblrole` (`Id`, `Name`, `Description`, `CreatedAt`, `CreatedBy`, `ModifiedAt`, `ModifiedBy`, `IsActive`) VALUES
(1, 'Admin', 'Admin', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 1, 1),
(2, 'User', 'User', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `Id` int(121) NOT NULL,
  `Firstname` varchar(250) NOT NULL,
  `Lastname` varchar(250) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `Birthdate` datetime NOT NULL,
  `Gender` int(11) NOT NULL,
  `contactno` varchar(20) NOT NULL,
  `SubjectId` int(11) NOT NULL,
  `Password` varchar(350) DEFAULT NULL,
  `salt` varchar(500) NOT NULL,
  `hashedPassword` varchar(2500) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `CreatedBy` bigint(121) DEFAULT 1,
  `ModifiedAt` datetime DEFAULT NULL,
  `ModifiedBy` bigint(121) NOT NULL DEFAULT 1,
  `IsActive` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`Id`, `Firstname`, `Lastname`, `Email`, `Birthdate`, `Gender`, `contactno`, `SubjectId`, `Password`, `salt`, `hashedPassword`, `CreatedAt`, `CreatedBy`, `ModifiedAt`, `ModifiedBy`, `IsActive`) VALUES
(1, 'Akshay', 'Pamnani', 'asp.1349@gmail.com', '0000-00-00 00:00:00', 0, '', 0, '', 'f14edd92ce9098123a95beb136c7b536', 'cb75eda57c24e5c0248cf21453233c5b9f105eed122326bf719c1346dbf24f6b190bf28ef981d6e3afb31543cafae69ab2785ad1378089cc7dd420ef3c317f25', NULL, 1, '2020-02-01 13:46:05', 1, 1),
(2, 'Akshay', 'Pamnani', 'asp.1349@gmail.com', '0000-00-00 00:00:00', 0, '', 0, '', 'c09ce966ba665ff7546813c3865ffea9', 'f448a50167b4768f668c52e551415ff64184f9f16feadd72fab48d3495f34b7707f90c6562d3c3cf7c06cce68d59cfdcededa50f8c692ad422da875084d40523', NULL, 1, '2020-02-01 13:46:12', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbluserrole`
--

CREATE TABLE `tbluserrole` (
  `Id` bigint(121) NOT NULL,
  `UserId` bigint(121) NOT NULL,
  `RoleId` bigint(121) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` bigint(121) NOT NULL,
  `ModifiedAt` datetime NOT NULL,
  `ModifiedBy` bigint(121) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbluserrole`
--

INSERT INTO `tbluserrole` (`Id`, `UserId`, `RoleId`, `CreatedAt`, `CreatedBy`, `ModifiedAt`, `ModifiedBy`) VALUES
(1, 1, 1, '2020-02-01 13:43:04', 1, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblrole`
--
ALTER TABLE `tblrole`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbluserrole`
--
ALTER TABLE `tbluserrole`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblrole`
--
ALTER TABLE `tblrole`
  MODIFY `Id` bigint(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `Id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbluserrole`
--
ALTER TABLE `tbluserrole`
  MODIFY `Id` bigint(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
