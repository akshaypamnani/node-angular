'use strict';

var express = require('express');
var controller = require('./common.controller');
var router = express.Router();

router.get('/:collection', controller.get);
router.get('/:collection/:id', controller.getById);
router.post('/:collection/:id', controller.update);
router.put('/:collection', controller.create);
router.delete('/:collection/:id', controller.destroy);
router.delete('/soft/:collection/:id', controller.softdestroy);

module.exports = router;

