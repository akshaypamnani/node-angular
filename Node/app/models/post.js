
'use strict';
var crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    Subject: DataTypes.STRING,
    Description: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblposts',
    timestamps: false,


  });
  Post.associate = function (models) {
  };
  return Post;
};