var express = require('express');
const mysql = require('mysql');
var bodyParser = require("body-parser");
var cors = require('cors');
var config = require('./app/config/config.js');
var app = express();

app.use(bodyParser.json());
app.use(cors({
    origin: function (origin, callback) {
        return callback(null, true);
    },
    optionsSuccessStatus: 200,
    credentials: true
}));
// Connecting to the database
const mc = mysql.createConnection({
    host: config.DB.host,
    user: config.DB.user,
    password: config.DB.password,
    database: config.DB.Livedatabase
});

// connect to database
mc.connect();

//Serves resources from public folder
app.use(express.static('statics'));

// Require routes
require('./app/routes/routes.js')(app);

// connection port set
app.listen(9090, () => {
    console.log("Server is listening on port 9090");
});